#!/usr/bin/env bash

clear

if [ "$(id -u)" != "0" ]; then
    echo "abrindo o arquivo $0 como administrador"
   sudo $0
fi

#dando permissao para todos os arquivos
sudo chmod 777 *

./config.sh

#movendo a pasta do programa para a pasta de programas em /opt/
sudo mv dvi_linux/ /opt/

#preparando as informacoes para a criacao do icone
app_name="DVI Linux"
app_description="Que q tah pegando?"
app_dir_name="dvi_linux"
app_exec="dvi.sh"
app_icon_dir="dvi.png"
#if more the one, then separate with a semicon
app_categories="Development"

CreateIconMenuAndDesktop(){

	nome_doc="$3.desktop"
	cd /usr/share/applications/
	sudo touch $nome_doc
	sudo echo "[Desktop Entry]" > $nome_doc
	sudo echo "Name=$1" >> $nome_doc
	sudo echo "GenericName=$2" >> $nome_doc
	sudo echo "Exec=/opt/$3/$4" >> $nome_doc
	sudo echo "Terminal=true" >> $nome_doc
	sudo echo "Icon=/opt/$3/$5" >> $nome_doc
	sudo echo "Type=Application" >> $nome_doc
	sudo echo "Categories=Application;Network;$6;" >> $nome_doc
	sudo echo "Comment=$2" >> $nome_doc
}

clear

CreateIconMenuAndDesktop "$app_name" "$app_description" "$app_dir_name" "$app_exec" "$app_icon_dir" "$app_categories"

echo "Pronto. Procure o programa pelo menu"