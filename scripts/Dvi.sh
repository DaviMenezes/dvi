#!/bin/bash

# ---- Davi Menezes -- #
# ---- Dvi Linux ----- #

#Cabecalho de apresentacao
Header(){
	clear
    echo " ____________________________________________________________________"
	echo "|                                                                    |"
	echo "| --------------- Bem Vindo ao Dvi Linux ----------------------------|"
	echo "| Central de ajuda com algumas coisas legais                         |"
	echo "| Para Sugestões, dúvida ou suporte, entre em contato pelo email     |"
	echo "| dvi.linux@gmail.com                                                |"
	echo "|____________________________________________________________________|"
	echo
}
Init(){
  Header
	echo "1 Central de Programas"
	echo "2 Central de Temas"
	echo "3 Manutenção"
	echo "s Sair"
	read opcao

	case ${opcao} in
		1)	Call ShowSoftwareCenter ;;
		2)	Call ShowThemeCenter ;;
		3) 	Call ShowSystemMaintenance ;;
		s)	Exit ;;
		*)	OptInvalid Init ;;
	esac
}


GetDistroWithPython(){
	python -c 'import platform; print(platform.dist()[0])' 
}

GetDistro(){
	var="$(lsb_release -is)"
	echo "Voceh gostaria de instalar o $1 ou " $var
}
#distro=`GetDistro Arch`
#echo $distro

Atualizar(){
	echo "Atualizando, aguarde"
    sudo apt-get update && sudo apt-get upgrade
}

Timestamp(){
	date +"%Y-%m-%d_%H-%M-%N"
}

OptInvalid(){

    txt="Opção inválida."

    #Run Function with text parameter
    $1 $txt
}

Cancel(){
	#1 Function
	#2 Information about cancelation

	Header

    if [ $# -gt 1 ]; then
    	echo -e "$2 Cancelado"
    fi

    #Run Function
    $1
}
Exit(){
	echo " ____________________________________________"
    echo "|---------------- DVI LINUX -----------------|"
    echo "|         Encontrou o que procurava?         |"
    echo "|           envie sua sugestão para          |"
    echo "|            dvi.linux@gmail.com             |"
    echo "|____________________________________________|"
	exit
}

fn_exists() {
  # appended double quote is an ugly trick to make sure we do get a string -- if $1 is not a known command, type does not output anything
  [ `type -t $1`"" == 'function' ]
}

#Responsavel por chamar e organizar a chamada de funcoes
#Usando: Call NomeDaFuncao
Call(){
    Header

    if [ $# -gt 0 ]; then
        #if [ fn_exists "$1" ]; then
            $1
        #elif [ -f "$1.sh" ]; then
            #./$1.sh
        #fi
    fi
}
