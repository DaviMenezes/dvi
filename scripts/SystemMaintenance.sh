#!/usr/bin/env bash

ShowSystemMaintenance()
{
	echo "O que deseja?"
	echo "1 Atualizar o Sistema"
	echo "2 Recuperar Login"
	echo "3 Encerrar Processos"
	echo "4 Recuperar Wifi"
	echo "s Sair"
	echo "v Volta para menu principal"
	read opcao

	case $opcao in
	1)
		Atualizar
		ShowSystemMaintenance ;;
	2)
		echo "Recuperando Login"

		#Limpar Configuracao do usuario
		Atualizar
		sudo rm -fr ~/.*

		#Arrumar pacotes quebrados
		sudo dpkg --configure -a
		sudo apt-get install -f

		ShowSystemMaintenance;;

	3)	#Listar processos
		echo "Informe o processo a ser incerrado"
		read proc
		#ex: akonadi
		pkill -f $proc*

		ShowSystemMaintenance;;
	4)
		echo "Instalando..."
		echo "Cria pasta dos aquivos que serao baixados"
		cd /tmp
		mkdir drivers_wifi_deb

		#Entra na pasta
		cd drivers_wifi_deb

		echo "Baixando drivers"
		wget http://dikdurl.tk/drivers_wifi_deb

		#Renomeando o arquivo baixado para o nome original
		mv drivers_wifi_deb drivers_wifi_deb.tar.gz

		echo "Descompactando os arquivos"
		tar -xf drivers_wifi_deb.tar.gz

		echo "Instalando os pacotes .deb"
		sudo dpkg -i *deb

		echo "Removendo o diretório e os arquivos baixados"
		cd /tmp
		rm -r drivers_wifi_deb

		ShowSystemMaintenance;;

	C|c|S|s) Exit ;;

    V|v) Init ;;

	*) OptInvalid ${FUNCNAME[ 0 ]} ;;

	esac
}