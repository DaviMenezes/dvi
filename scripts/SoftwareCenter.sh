#!/usr/bin/env bash

#--------------------------------------------------------------------------------------------------
#---- SoftwareCenter ------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------

function ShowSoftwareCenter {

	#p=$(whiptail --title "Bem Vindo ao Dvi Linux" --checklist --fb \
	#"Selecione os programas que deseja" 15 80 8 \
	#"WizNote"	"" OFF \
    #"Openproject - Escritório" "" OFF \
	#"Ligthshot - Gráfico - Capturar Tela" "" OFF \
	#"Tox chat - Internet - Bate papo criptografado" "" OFF \
	#"MegaSync - Cloud - mega.nz" "" OFF \
	#"Telegram" "" OFF \
	#"Cancel" "" ON \
	#"Voltar ao menu principal" "" OFF 3>&1 1>&2 2>&3)
	#status=$?
	echo " ______________________________________________________________"
    echo "|                                                              |"
    echo "|-------------------- Central de Programas --------------------|"
    echo "|______________________________________________________________|"
	echo
	echo "Selecione os programas que deseja"
	echo "1 WizNote"
    echo "2 Openproject - Escritório"
	echo "3 Ligthshot - Gráfico - Capturar Tela"
	echo "4 Tox chat - Internet - Bate papo criptografado"
	echo "5 MegaSync - Cloud - mega.nz"
	echo "6 Telegram"
	echo "7 Kivy"
	echo "8 Sublime Text"
	echo "9 Xfce4 minimal"
	echo "10 App Grid - Alternativa à Central de programas (gráfico) do ubuntu"
	echo "11 Whisker Menu"
	echo "12 Butter"
	echo "13 Stremio"
	echo "14 Atalho para brilho da tela"
	echo "15 QGifer - Converta Videos para Gif"
    echo "16 StreamStudio"
    echo "17 Visual Studio Code"
    echo "18 Pepper Flash Player - Permite a visualização de vídeos e outros conteúdos em flash"
    echo "19 Notepadqq - Editor de Texto"
    echo "20 Kdenlive - Editor de vídeo"
    echo "21 OpenShot - Editor de vídeo"
    echo "22 Youtube DLG - Baixar vídeos da internet"
	echo "c Cancel"
	echo "v Voltar ao menu principal"
	read programas

	case $programas in
		1)
			txt="Instalando WizNote."
			read -p "$txt. Continuar? [s|n] "
			case $REPLY in
			    N|n) Cancel ${FUNCNAME[ 0 ]} "$txt";;
			    Y|y|S|s)
                    echo "Instalando..."
                    echo
                    sudo add-apt-repository ppa:wiznote-team
                    sudo apt-get update
                    sudo apt-get install wiznote ;;
                C|c) Cancel ${FUNCNAME[ 0 ]} $txt ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
		2)
			txt="Intalando Openproject"
			read -p "$txt. Continuar? [s|n]"

			case $REPLY in
                N|n) Cancel ${FUNCNAME[ 0 ]} "$txt";;

                Y|y|S|s)
                    echo "Aguarde..."
                    echo
                    wget -qO - https://deb.packager.io/key | sudo apt-key add -
                    echo "deb https://deb.packager.io/gh/finnlabs/pkgr-openproject-community trusty stable/4.2" | sudo tee /etc/apt/sources.list.d/pkgr-openproject-community.list
                    sudo apt-get update
                    sudo apt-get install openproject-ce ;;

			    *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
		3)
			txt="Instalando Lightshot"
			read -p "$txt. Continuar? [s|n] "

			case $REPLY in
			    N|n) Cancel ${FUNCNAME[ 0 ]} "$txt";;

                Y|n|S|s)
                    echo "Aguarde..."
                    sudo apt-get install wine
                    wget http://app.prntscr.com/build/setup-lightshot.exe
                    wine ./setup-lightshot.exe ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
			esac ;;
		4)
			echo "Informe o seu sistema operacional"
			echo "1 Debian, Ubuntu, Mint, etc."
			echo "2 Gentoo"
			echo "3 Arch"
			echo "c Cancel"
			echo "v Voltar para o menu principal"
			read opcao
			case $opcao in
				1)
					txt="Instalando Tox para família Debian"
					read -p "$txt. Continuar ? [s|n] "

					case $REPLY in
					    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;

					    Y|y|S|s)
                            echo "deb https://pkg.tox.chat/debian nightly release" | sudo tee /etc/apt/sources.list.d/tox.list
                            wget -qO - https://pkg.tox.chat/debian/pkg.gpg.key | sudo apt-key add -
                            sudo apt-get install apt-transport-https
                            sudo apt-get update
                            echo "Está usando Unity ? [s|n] Defaul is not "

                            case $REPLY in
                                Y|y|S|s)    sudo apt-get install qtox-unity ;;
                                N|n)        sudo apt-get install qtox ;;
                                c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                                *)          OptInvalid ${FUNCNAME[ 0 ]} ;;
                            esac ;;
                    esac ;;
				2)
					txt="Instalando Tox para Gentoo"
					ready -p "$txt. Continuar ? [s|n] "

					case $REPLY in
					    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;

					    Y|y|S|s)
                            echo "The Tox overlay is now in the official layman list."
                            layman -a tox-overlay ;;
                        *) OptInvalid ShowSoftwareCenter ;;
					esac ;;

				3)
					txt="Instalando Tox para Arch"
					read -p "$txt. Continuar ? [s|n] "

					case $REPLY in
					    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;

					    Y|y|S|s)
						    echo "Many Tox related packages exist in the AUR. However, if you have an issue with Tox and you installed Tox using the AUR packages, please compile that package from source before filing a bug report. If the issue appears to be on the Arch maintainers side (eg: compiles from source fine, but the PKGBUILD fails), please contact the package maintainer and let them know!" ;;
					    *) OptInvalid ShowSoftwareCenter ;;
					esac ;;

				c)  Cancel ;;

				v)  Init ;;

				*) OptInvalid ${FUNCNAME[ 0 ]} ;;
			esac ;;
		5)
		    app_name="Megasync"
			echo "Instalando $app_name. Continuar? [s/n] "
			read -p ""
			case $REPLY in
			    S|s)
			        echo "Informe a distribuição"
                    echo "1 Ubuntu 64"
                    echo '2 Ubuntu 32'
                    echo "3 Mint 17 64"
                    echo "4 Mint 17 32"
                    read dist

                    cd /tmp
                    tmp_dir="megasync_download"
                    mkdir $tmp_dir
                    cd $tmp_dir

                    case $dist in
                        1 )
                            DownloadInstallApp "https://mega.nz/linux/MEGAsync/xUbuntu_15.10/amd64/megasync-xUbuntu_15.10_amd64.deb" ".deb"
                            ${FUNCNAME[ 0 ]} "$app_name Instalado" ;;
                        2 )
                            app="megasync-xUbuntu_15.10_i386.deb"
                            DownloadInstallApp "https://mega.nz/linux/MEGAsync/xUbuntu_15.10/i386/$app" ".deb"
                            ${FUNCNAME[ 0 ]} "$app_name Instalado" ;;
                        3)
                            app="megasync-xUbuntu_14.04_amd64.deb"
                            DownloadInstallApp "https://mega.nz/linux/MEGAsync/xUbuntu_14.04/amd64/$app" ".deb"
                            ${FUNCNAME[ 0 ]} "$app_name Instalado" ;;
                        4)
                            app="megasync-xUbuntu_14.04_i386.deb"
                            DownloadInstallApp "https://mega.nz/linux/MEGAsync/xUbuntu_14.04/i386/$app" ".deb"
                            ${FUNCNAME[ 0 ]} "$app_name Instalado" ;;
                        C|c)
                            Cancel ${FUNCNAME[ 0 ]} ;;
                        *)
                            OptInvalid ${FUNCNAME[ 0 ]} ;;
                    esac ;;
                N|n)
                    Cancel ${FUNCNAME[ 0 ]} "$app_name";;
                *)
                    OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
		6)
		    txt="Instalando Telegram."
		    read -p "$txt Continuar? [s/n]"
		    case $REPLY in
                N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                Y|y|S|s)
                    dirApp="Apps"
                    cd ~
                    if [ ! -d $dirApp ]; then
                        mkdir $dirApp
                        cd $dirApp
                    fi

                    dir_="telegram_"`Timestamp`
                    mkdir $dir_
                    cd $dir_
                    wget https://tdesktop.com/linux
                    mv * telegram.tar.xz
                    tar -xf *.tar.xz

                    sudo mv Telegram ~/$dirApp/

			        cd ~/$dirApp/Telegram/
			        rm -r ~/dirApp/$dir_

			        ./Telegram ;;
                V|v) Init ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
		7)
		    txt="Instalando Kivy."
			echo "Qual o seu sistema?"
			echo "1 Ubuntu / Kubuntu / Xubuntu / Lubuntu (Saucy and above)"
			echo "2 Debian (Jessie or newer)"
			echo "c Cancel"
			echo "v Voltar ao menu"
			echo "i Voltar ao Início"
			read "opt"
			case $opt in
				1)
				    read -p "$txt para Ubuntu. Continuar? [s|n] "
					case $REPLY in
					    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
					    Y|y|S|s)
                            #Adicionando repositorio
                            sudo add-apt-repository ppa:kivy-team/kivy
                            #instalando para python 3
                            sudo apt-get install python3-kivy

                            read -p "Instalar exemplos? [s/n] Padrão é n"
                            case $REPLY in
                                S|s) sudo apt-get install kivy-examples ;;
                                n|N) exit ;;
                                N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
                            esac ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
                    esac ;;
				2)
				    read -p "$txt para Debian. Continuar? [s|n] "
					case $REPLY in
					    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
					    Y|y|S|s)

                            # Adicionando PPAs com synaptic -------------------------------
                            echo "1 Jessie/Testing"
                            echo "2 Sid/Unstable"
                            echo "c Cancel"
                            echo "v Voltar ao menu"
                            echo "i Voltar ao início"
                            echo "Informe a versão do sistema"
                            read opt
                            case $opt in
                                1) deb http://ppa.launchpad.net/kivy-team/kivy/ubuntu trusty main ;;
                                2) deb http://ppa.launchpad.net/kivy-team/kivy/ubuntu utopic main ;;
                                c) Cancel ;;
                                v) ShowSoftwareCenter ;;
                                i) Init ;;
                            esac

                            echo "Add the GPG key to your apt keyring by executing"
                            echo "*Adicionar chave para"
                            echo "1 $USER"
                            echo "2 ROOT"
                            echo "c Cancel"
                            echo "v Voltar ao menu'"
                            echo "i Voltar ao inínio"
                            read opt

                            case $opt in
                                1) sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A863D2D6 ;;
                                2) apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A863D2D6 ;;
                                c) Cancel ;;
                                v) ShowSoftwareCenter ;;
                                i) Init ;;
                            esac
                            # fim de Adicionando PPAs com synaptic -------------------------------

                            # Intalando ----------------------------------------------------------
                            sudo apt-get update
                            sudo apt-get install python3-kivy

                            #Instalando exemplos--------------------------------------------------
                            echo "Instalar exemplos? [s/n] Padrao é n"
                            read opt
                            case $opt in
                                S | s) sudo apt-get install kivy-examples ;;
                                n | N) Cancel ;;
                                *) echo "Opção inválida";  ShowSoftwareCenter ;;
                            esac ;;

                            *) OptInvalid ${FUNCNAME[ 0 ]}
                        esac ;;
				i) Init ;;
				*) OptInvalid ${FUNCNAME[ 0 ]} ;;
			esac ;;
		8)
		    txt="Instalando Sublime Text."
		    read -p "$txt Continuar? [s|n] "
			case $REPLY in
			    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
			    Y|y|S|s)
                    cd /tmp
                    wget http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2%20x64.tar.bz2
                    tar -xjf Sublime*
                    sudo mv "Sublime Text 2" /opt/sublime_text_2
                    rm Sublime\ Text\ 2*

                    CreateIconMenuAndDesktop "Sublime Text 2" "Text Editor" "sublime_text_2" "sublime_text" "Icon/48x48/sublime_text.png" "Development" ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
		9)
		    txt="Instalando XFCE4."
		    read -p "$txt Continuar? [s|n] "
			case $REPLY in
			    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
			    Y|y|S|s)
                    echo "Baixando instalador..."
                    wget http://dikdurl.tk/installXfce4
                    echo "Aplicando permissões..."
                    sudo chmod +x installXfce4
                    echo "Iniciando instalador"
                    sudo ./installXfce4
                    echo "Pronto." ;;
                *)  OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
		10)
		    txt="Instalando App Grid."
			read -p "$txt Continuar? [s|n] "
			case $REPLY in
			    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
			    Y|y|S|s)
                    cd /tmp
                    mkdir appgrid_0.252_all
                    cd appgrid_0.252_all
                    wget http://ppa.launchpad.net/appgrid/stable/ubuntu/pool/main/a/appgrid/appgrid_0.252_all.deb
                    sudo dpkg -i *.deb
                    sudo rm -r /tmp/appgrid_0.252_all
                    echo "Pronto." ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
		11)
		    txt="Instalando Whisker Menu."
			read -p "$txt Continuar? [s|n] "
			case $REPLY in
			    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
			    Y|y|S|s)
                    sudo add-apt-repository ppa:gottcode/gcppa
                    sudo apt-get update
                    sudo apt-get install xfce4-whiskermenu-plugin
                    echo "Pronto..." ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac;;
		12)
		    txt="Instalando Butter."
		    read -p "$txt Continuar? [s|n] "
			case $REPLY in
			    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
			    Y|y|S|s)
                    echo "Este serviço está em manutenção. Aguarde novidades"
                    exit
                    cd /opt
                    sudo git clone https://github.com/butterproject/butter.git203
                    cd butter
                    sudo apt-get install npm nodejs-legacy
                    sudo npm install -g grunt-cli bower
                    ./make_butter.sh ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac;;
		13)
		    txt="Instalando Stremio."
			read -p "$txt Continuar? [s/n]"
			case $REPLY in
				N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
				S|s)
					echo "Preparando diretórios"
					cd /tmp

					temp_dir="stremio_"`Timestamp`
					dir_stremio="stremio2"
					mkdir $temp_dir
					cd $temp_dir

					mkdir $dir_stremio
					cd $dir_stremio

					echo "Baixando..."
					wget http://dl.strem.io/Stremio3.3.3.linux.tar.gz
					echo "Descompactando..."
					tar -xf Stremio3.3.3.linux.tar.gz

					echo "Excluindo arquivos temporários"

					#excluindo arquivo zip antes de mover
					sudo rm Stremio3.3.3.linux.tar.gz

					#movendo a pasta para dirketorio de programas
					cd ..
					sudo mv $dir_stremio/ /opt

                    RemoveTempDir $dir_stremio $temp_dir ;;

				*) OptInvalid ${FUNCNAME[ 0 ]} ;;
			esac ;;
		14)
		    txt="Instalando atalho para controle do brilho da tela."
		    read -p "$txt Continuar? [s|n] "
			case $REPLY in
			    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
			    Y|y|S|s)
                    echo "Instalador em manutenção. Aguarde"
                    exit

                    cd opt/
                    mkdir AumDimBrilho
                    cd AumDimBrilho

                    sudo apt-get install xblacklight

                    touch diminuiBrilho.sh
                    echo "#!/bin/sh" > "diminuiBrilho.sh"
                    echo "xbacklight -dec 10" >> "diminuiBrilho.sh"
                    chmod +x .diminuiBrilho.sh

                    touch aumentaBrilho.sh
                    echo "#!/bin/sh" > "aumentaBrilho.sh"
                    echo "xbacklight -inc 10" >> "aumentaBrilho.sh"
                    chmod +x .diminuiBrilho.sh ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
        15) #fonte: http://dikdurl.tk/1NaIX3r
            app="QGifer"
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                I|i)
                    txt="Instalação de $app."
                    read -p "$txt Continuar? [s/n] "

                    case $REPLY in
                        Y|y|S|s)
                            echo "Adicionando repositorio"
                            sudo add-apt-repository ppa:nilarimogard/webupd8
                            echo "Atualizando"
                            sudo apt-get update
                            echo "Instalando"
                            sudo apt-get install qgifer ;;

                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;

                        *) OptInvalid ${FUNCNAME[ 0 ]}
                    esac ;;
                R|r)
                    txt="Remoção de $app"
                    read -p "$txt Continuar? [s/n] "
                    case $REPLY in
                        S|s)
                            sudo apt-get remove qgifer
                            sudo apt-get autoremove ;;

                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                        *) OptInvalid ${FUNCNAME[ 0 ]}
                    esac ;;
            esac ;;
        16)
            app="StremStudio"
            action=''
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                Y|y|I|i)
                    action="Instalação de $app"
                    sudo add-apt-repository "deb http://ppa.streamstudio.me/  $(lsb_release -cs) main"
                    sudo apt-get update
                    sudo apt-get install streamstudio
                    echo "$action concluída" ;;
                R|r)
                    action="Remoção de $app"
                    sudo apt-get remove streamstudio
                    echo "$action concluída" ;;
                C|c) Cancel ${FUNCNAME[ 0 ]} "$app" ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} "$action" ;;
            esac ;;
        17)
            app="Visual Studio Code"
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                I|i)
                    echo "Instalando $app"
                    cd /tmp
                    tmp_dir="vscode_"`Timestamp`
                    mkdir $tmp_dir
                    cd $tmp_dir
                    wget https://az764295.vo.msecnd.net/public/0.10.2/VSCode-linux64.zip
                    unzip VSCode-linux64.zip
                    sudo mv VSCode-linux-x64 /opt/

                    RemoveTempDir "/tmp/$tmp_dir/VSCode-linux-x64" $tmp_dir

                    CreateIconMenuAndDesktop $app "Text Editor" "vs_studio_code" "Code" "/opt/VSCode-linux-x64/resources/app/resources/linux/vscode.png" "Development" ;;

                R|r)
                    echo "Removendo $app"
                    sudo apt-get remove -purgle vs_studio_code;;

                *) OptInvalid ${FUNCNAME[ 0 ]} "$action"

            esac ;;
        18)
            app="Pepper Flash Player"
            InstallRemove "$app"
            reap -p ""
            case $REPLY in
                I|i)
                    echo "Instalando $app"
                    sudo apt-get install browser-plugin-freshplayer-pepperflash ;;
                R|r)
                    echo "Removendo $app"
                    sudo apt-get remove browser-plugin-freshplayer-pepperflash ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} "$action"
            esac ;;
        19 )
            app="Notepadqq para Debian e derivados"
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                I|i)
                    echo "Instalando $app"
                    cd /tmp
                    tmp_dir "notepadqq_"`Timestamp`

                    #sudo add-apt-repository ppa:notepadqq-team/notepadqq
                    #sudo apt-get update
                    #sudo apt-get install notepadqq

                    mkdir $tmp_dir && cd $tmp_dir
                    wget -c https://launchpad.net/~notepadqq-team/+archive/ubuntu/notepadqq/+build/8757531/+files/notepadqq_0.50.6-0~wily1_amd64.deb
                    sudo dpkg -i *.deb

                    rm -rf /tmp/$tmp_dir ;;
                R|r)
                    echo "Removendo $app"
                    sudo apt-get remove -purge notepadqq;;

                *) OptInvalid ${FUNCNAME[ 0 ]} "$action"

            esac ;;
        20)
            app="Kdenlive para Ubuntu derivados"
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                I|i)
                    echo "Instalando $app"
                    sudo add-apt-repository ppa:sunab/kdenlive-release
                    sudo apt-get update
                    sudo apt-get install kdenlive
                    echo "$app Instalado"
                    ${FUNCNAME[ 0 ]} ;;

                R|r)
                    sudo apt-get remove --purge kdenlive* ;;
                C|c)
                    Cancel ${FUNCNAME[ 0 ]} ;;
                *)
                    OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
        21)
            echo "Ubuntu 9.10 (Karmic) and above"
            app="OpenShot Editor de vídeo"
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                I|i)
                    sudo add-apt-repository ppa:openshot.developers/ppa
                    sudo apt-get update
                    sudo apt-get install openshot openshot-doc
                    ${FUNCNAME[ 0 ]} "$app Instalado" ;;
                R|r)
                    sudo apt-get remove --purge openshot openshot-doc
                    ${FUNCNAME[ 0 ]} "$app Removido" ;;
                C|c)
                    Cancel ${FUNCNAME[ 0 ]} ;;
                *)
                    OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
        22)
            app="Youtube-dl-gui"
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                I|i)
                    echo "Modo de Instalação"
                    echo "1 - Via ppa"
                    #echo "2 - Via instalador"
                    read -p ""
                    case $REPLY in
                        1)
                            sudo add-apt-repository ppa:nilarimogard/webupd8
                            sudo apt-get update
                            sudo apt-get install youtube-dlg

                            ${FUNCNAME[ 0 ]} "$app Instalado" ;;
                        2)
                            #Está dando erro ao instalar desta forma
#                            cd /tmp
#                            tmp_dir="$app"`Timestamp`
#                            mkdir $tmp_dir
#                            cd $tmp_dir
#
#                            wget -c http://ppa.launchpad.net/nilarimogard/webupd8/ubuntu/pool/main/y/youtube-dlg/youtube-dlg_0.3.8.orig.tar.gz
#                            tar xvzf youtube-dlg_0.3.8.orig.tar.gz
#                            cd youtube-dl-gui-0.3.8
#                            sudo aptitude install python-wxgtk3.0-dev
#                            sudo python setup.py install
#
#                            cd /tmp
#                            rm -rf $tmp_dir
#
#
#                            ${FUNCNAME[ 0 ]} "$app Instalado"
                            ;;
                        *)
                            OptInvalid ${FUNCNAME[ 0 ]}
                    esac ;;

                R|r)
                    sudo apt-get remove --purge youtube-dlg
                    ${FUNCNAME[ 0 ]} "$app Removido" ;;
                C|c)
                    Cancel ${FUNCNAME[ 0 ]} ;;
                *)
                    OptInvalid ${FUNCNAME[ 0 ]} ;;
             esac ;;

		C|c) Cancel ${FUNCNAME[ 0 ]} ;;
		V|v) Init ;;
		*) OptInvalid ${FUNCNAME[ 0 ]} ;;
	esac
}

DownloadInstallApp(){

    app=$1
    type=$2
    wget $app

    case $type in
        ".deb")
            sudo gdebi $app ;;
        *)
            echo "Tipo de arquivo não informado" ;;
    esac
}

RemoveTempDir(){
    #A cada 1 segundo é verificado se determinada pasta ainda existe, e
    #caso não exista, significa que o programa já foi movido para seu devido lugar.
    #Então a pasta termporaria onde ele estava armazenado, ja pode ser removida
    while sleep 1s; do
        if [ ! -e $1 ]; then
            sudo rm -rf /tmp/$2
            exit
        fi
    done
}
CreateIconMenuAndDesktop(){
	nome_doc="$3_.desktop"
	cd /usr/share/applications/
	sudo touch $nome_doc
	sudo echo "[Desktop Entry]" > $nome_doc
	sudo echo "Name=$1" >> $nome_doc
	sudo echo "GenericName=$1" >> $nome_doc
	sudo echo "Exec=/opt/$3/$4" >> $nome_doc
	sudo echo "Terminal=false" >> $nome_doc
	sudo echo "Icon=/opt/$3/$5" >> $nome_doc
	sudo echo "Type=Application" >> $nome_doc
	sudo echo "Categories=Application;Network;$6;" >> $nome_doc
	sudo echo "Comment=$2" >> $nome_doc
}

InstallRemove() {
    echo "$1"
	echo "i Instalar"
	echo "r Remover"
	echo "c Cancelar"
}

#Cria e entra no diretorio se nao existir
#param2 = FolderName
#param3 = url_download
InstallApp(){
    folder=$2
    url=$3
    dir_temp="download_$folder"

    cd /tmp
    mkdir $dir_temp
    cd $dir_temp

    #verifica se o arquivo ja existe para não precisar baixar novamente
    #Todo: verificar versão para saber se precisa baixar novamente
    #Todo: Nem todo download vem com extensão .zip, portanto criar uma verificação mais genérica
    if [ ! -f $folder.zip ]; then
        wget -O $folder.zip $url
    fi

    unzip $folder.zip

    mv */ $folder
    mv $folder /usr/share/icons/

}
#Se for passado algum parametro/funcao, verifico se existe para entao executa-la
#if [ $# -gt 0 ] && [ fn_exists "$1" ]; then
#    $1
#else
#    ShowSoftwareCenter
#fi
