#!/usr/bin/env bash
ShowThemeCenter(){

	echo
	echo $1 #Parametro com alguma string informativa opicional
	echo
	echo "Instalar Temas"
	echo
	echo "1 Arc"
	echo "2 Vibrance Colors"
	echo "3 Masalla icon"
	echo "4 Flattastic 14-03-11"
	echo "5 Ambiance 14-1-04"
	echo "6 Icones Vivacious Colors gtk"
	echo "7 Temas para GIMP Flat V2"
	echo "8 Tema de ícones Evolvere para Ubuntu e Derivados"
	echo "9 Transformar no Windows"
	echo "10 Paper"
	echo "11 nouveGnomeGray"
	echo "12 Tema de ícones Flatabulous"
	echo "13 Tema Vertex"
	echo "14 Tema de ícones Shadow"
	echo "15 Tema par Gnome Ceti-2-theme"
	echo "v Voltar ao menu principal"
	echo "c Cancel"
	echo "Informe o Tema desejado"
	read theme

	case $theme in
        1)	InstallThemeArc	;;
        2)
            txt="Instalando Vibrance Colors."
            read -p "$txt Continuar? [s|n] "
            case $REPLY in
                N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                Y|y|S|s)
                    sudo add-apt-repository ppa:ravefinity-project/ppa
                    sudo apt-get update
                    sudo apt-get install vibrancy-colors ;;
            esac ;;
        3)
            txt="Instalando Masalla."
            read -p "$txt Continuar? [s|n] "
                case $REPLY in
                    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                    Y|y|S|s)
                        echo "Baixando os ícones..."
                        cd /tmp
                        if [ ! -f "master.zip" ]; then
                            wget https://github.com/hayderctee/masalla-icon-theme/archive/master.zip;
                        fi

                        echo "Descompactando..."
                        unzip master.zip

                        echo "Intalando os ícones..."
                        sudo mv masalla-icon-theme-master /usr/share/icons/
                        #atualizando cache dos icones
                        sudo gtk-update-icon-cache /usr/share/icons/masalla-icon-theme-master/

                        echo "Removendo arquivos"
                        sudo rm master.zip

                        echo "Pronto" ;;

                    *)	echo "Opção inválida"; ShowThemeCenter ;;
            esac ;;
        4)
            txt="Instalando Flattastic."
            read -p "$txt Continuar? [s|n] "
            case $REPLY in
                N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt" ;;
                Y|y|S|s)
                    cd /tmp
                    mkdir flattastic
                    cd flattastic
                    wget http://orig13.deviantart.net/5670/f/2014/070/6/d/flattastic_11_03_2014_by_nale12-d70zd5z.zip
                    sudo mv flattastic* flattastic.zip
                    unzip flattastic.zip
                    sudo mv Flattastic*/ /usr/share/themes/
                    cd /tmp
                    sudo rm -r flattastic
                    echo "Pronto." ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
        5)
            txt="Instalando Anbiance Theme."
            read -p "$txt Continuar? [s|n] "
                case $REPLY in
                    N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                    Y|y|S|s)
                        echo "em manutenção..."

                        Call Cancel

                        cd /tmp
                        mkdir ambiancetheme
                        cd ambiancetheme
                        echo "Baixando arquivos..."
                        wget https://d6.usercdn.com/d/xylqp75wt52fvxijntri3vapanoiy3jxv3622xtbxbhvfal3byixe75m/ambiance-themes-14.1.04-1.tar.gz
                        echo "Descompactando..."
                        tar -vzxf *tar.gz
                        sudo mv ambiance*/ /usr/share/themes/
                        cd /tmp
                        echo "Excluindo arquivos desnecessários..."
                        sudo rm -r ambiancetheme
                        echo "Pronto." ;;
                    *) OptInvalid ${FUNCNAME[ 0 ]} ;;
                esac ;;
        6)
            txt="Instalando Vivacious - Icones de tema."
            read -p "$txt Continuar? [s|n] "
            case $REPLY in
                N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                Y|y|S|s)
                    echo "1 Instalar via ppa"
                    echo "2 Instalar pacote .deb"
                    echo "c Cancelar"
                    echo "v Voltar ao início"
                    echo "O padrão é ppa"
                    read opcao
                    case $opcao in
                        1)
                            echo "Instalando via ppa...";

                            sudo add-apt-repository ppa:ravefinity-project/ppa
                            sudo apt-get update
                            sudo apt-get install vivacious-colors

                            ShowThemeCenter Pronto ;;
                        2)
                            cd /tmp

                            temp_dir="vivacious-"`Timestamp`
                            mkdir $temp_dir
                            cd $temp_dir

                            wget https://launchpad.net/~ravefinity-project/+archive/ubuntu/ppa/+files/vivacious-colors_1.3~trusty~NoobsLab.com_all.deb
                            sudo gdebi vivacious-colors_1.3~trusty~NoobsLab.com_all.deb

                            echo "Apagando arquivos e diretórios obsoletos..."
                            cd /tmp
                            rm -r $temp_dir

                            ShowThemeCenter Pronto ;;

                        C|c) Cancel ShowThemeCenter;;
                        V|v) Init;;

                    esac ;;
            esac ;;
        7)
            #baixando arquivos
            wget http://orig14.deviantart.net/0ee4/f/2015/118/5/7/flat_gimp_icon_theme_v_2_1_by_android272-d679s6z.zip -O flat-gimp-icon.zip
            #descompactando arquivos
            unzip flat-gimp-icon.zip
            #movendo a pasta Decor para a pasta de temas do gimp a nivel de usuario
            mv Flat GIMP icon Theme V2/Decor/ ~/.gimp-2.8/themes/
            #movendo o conteudo da pasta gim orange para a pasta do gimp a nivel de usuario
            mv Flat GIMP icon Theme V2/GIMP_Orange/* ~/.gimp-2.8/themes/

            #reinicia o GIMP
            #Por @Ricardo Lobo
            #------------------
            ps -C gimp-2.8 > /dev/null

            if [ $? = 0 ]
            then
                echo "O Gimp esta em execução, deseja reiniciar o Gimp agora ?"
                killall -i gimp-2.8;
                gimp
            fi

            ;;
        8)
            txt="Tema de ícone Evolvere"
            InstallRemove $txt
            read -p ""
            case $REPLY in
                Y|y|I|i)
                    sudo add-apt-repository ppa:noobslab/evolvere
                    sudo apt-get update
                    sudo apt-get install evolvere-icon-suite
                    echo "Pronto. Selecione o tema nas configurações do seu sistema" ;;
                R|r)
                    sudo apt-get remove evolvere-icon-suite
                    echo "Pronto. Acões aplicadas";;
            esac ;;
        9)
            app="Transformar em windows"
            action=""
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                Y|y|I|i)
                    action="Instalando $app"
                    #add theme
                    sudo add-apt-repository ppa:noobslab/themes
                    sudo apt-get update
                    sudo apt-get install mindows-transformation-themes

                    #add icons
                    sudo add-apt-repository ppa:noobslab/icons
                    sudo apt-get update
                    sudo apt-get install victory-icon-theme

                    echo "$action ... Concluído" ;;
                R|r)
                    action="Removendo $app"
                    sudo apt-get remove mindows-transformation-themes victory-icon-theme
                    echo "$action ... Concluído" ;;
                C|c) Cancel ${FUNCNAME[ 0 ]} "$action" ;;
            esac ;;
        10)
            app="Paper"
            action=""
            InstallRemove "$app"
            read -p ""
            case $REPLY in
                Y|y|I|i)
                    action="Instalando $app"
                    echo ${action}
                    sudo add-apt-repository ppa:snwh/pulp -y
                    sudo apt-get update
                    sudo apt-get install paper-icon-theme paper-gtk-theme -y ;;
                R|r)
                    action="Removendo $app"
                    echo ${action}
                    sudo apt-get remove paper-icon-theme paper-gtk-theme ;;
                C|c) Cancel ${FUNCNAME[ 0 ]} "$app" ;;
                *) OptInvalid ${FUNCNAME[ 0 ]}
            esac ;;
        11)
            read -p "Instalando tema de ícones nouveGnomeGray. Continuar ? [n|y]"
            case $REPLY in
                Y|y|S|s)
                    InstallThemeIcons "nouveGnomeGray" http://dikdurl.tk/nouveGnomeGray
                    echo "Pronto. Use seu gerenciador de tema favorito para selecioná-lo" ;;
                N|n|C|c) Call Cancell ${FUNCNAME[ 0 ]} ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;

        12)
            read -p "Instalando tema de ícones Flatabulous via ppa:noobslab. Continuar? [s|n]"
            case $REPLY in
                S|s)
                    sudo add-apt-repository ppa:noobslab/icons
                    sudo apt-get update
                    echo "Escolha entre as cores- 1)Axul, 2)Laranja, 3)Verde"
                    read -p ""
                    case $REPLY in
                        1) sudo apt-get install ultra-flat-icons ;;
                        2) sudo apt-get install ultra-flat-icons-orange ;;
                        3) sudo apt-get install ultra-flat-icons-green ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
                    esac ;;
                N|n) Call Cancel ;;
            esac ;;
        13)
            read -p "Instalando Tema Vertex. Continuar? [s|n] "
            case $REPLY in
                S|s)
                    echo "Escolha sua versão do ubuntu"
                    echo "1) 15.10"
                    echo "2) 15.04"
                    echo "3) 14.10"
                    echo "4) 14.04"
                    read -p ""
                    case $REPLY in
                        1)
                            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_15.10/ /' >> /etc/apt/sources.list.d/vertex-theme.list"
                            sudo apt-get update
                            sudo apt-get install vertex-theme ;;
                        2)
                            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_15.04/ /' >> /etc/apt/sources.list.d/vertex-theme.list"
                            sudo apt-get update
                            sudo apt-get install vertex-theme ;;
                        3)
                            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_14.10/ /' >> /etc/apt/sources.list.d/vertex-theme.list"
                            sudo apt-get update
                            sudo apt-get install vertex-theme ;;
                        4)
                            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_14.04/ /' >> /etc/apt/sources.list.d/vertex-theme.list"
                            sudo apt-get update
                            sudo apt-get install vertex-theme ;;
                        *)
                            OptInvalid ${FUNCNAME[ 0 ]} ;;
                    esac ;;
                N|n)
                    Call Cancel ;;
                *)
                    OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
        14)
            echo "Instalando Tema de ícones Shadow via ppa noobslab."
            echo "Ubuntu 15.10 Wily/15.04 Vivid/14.04 Trusty/12.04 Precise/Linux Mint 17.x/17/13 e outros"
            echo "Continuar? [s/n] "
            read -p ""
            case $REPLY in
                S|s)
                    sudo add-apt-repository ppa:noobslab/icons
                    sudo apt-get update
                    sudo apt-get install shadow-icon-theme ;;
                N|n)
                    Call Cancel ;;
                *)
                    OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;
        15)
            echo "Instalando Tema ceti-2-theme Continuar? [s/n] "
            read -p ""
            case $REPLY in
                S|s)
                    echo "Informe sua Distribuição"
                    echo "1) Ubuntu 15.10"
                    reap -p ""
                    case $REPLY in
                        1)
                            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_15.10/ /' >> /etc/apt/sources.list.d/ceti-2-theme.list"
                            sudo apt-get update
                            sudo apt-get install ceti-2-theme

                            reap -p "Adicionar chave? [s/n] "
                            case $REPLY in
                                S|s)
                                    wget http://download.opensuse.org/repositories/home:Horst3180/xUbuntu_15.10/Release.key
                                    sudo apt-key add - < Release.key ;;
                                N|n)
                                    Call Cancell ;;
                                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
                            esac

                            ${FUNCNAME[ 0 ]} ;;

                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
                    esac ;;
                N|n|C|c)
                    Call Cancell ;;

                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
            esac ;;

        V|v)	Init ;;
        C|c)	Call Cancel ;;
        *) OptInvalid ${FUNCNAME[ 0 ]} ;;

	esac
}

#Cria e entra no diretorio se nao existir
#param2 = FolderName
#param3 = url_download
InstallThemeIcons(){
    folder=$2
    url=$3
    dir_temp="download_$folder"

    cd /tmp
    mkdir $dir_temp
    cd $dir_temp

    #verifica se o arquivo ja existe para não precisar baixar novamente
    #Todo: verificar versão para saber se precisa baixar novamente
    if [ ! -f $folder.zip ]; then
        wget -O $folder.zip $url
    fi

    unzip $folder.zip

    mv */ $folder
    mv $folder /usr/share/icons/

}

InstallThemeArc(){
	echo "Tema Arc"
	echo "Informe sua distribuição"
	echo
	echo "1 Ubuntu"
	echo "2 openSuse"
	echo "3 Fedora"
	echo "4 Debian"
	echo "v Voltar ao menu principal"
	echo "c Cancel"
	read opcao

	case $opcao in
		1)
			echo "Informe a versão"
			echo "1 xUbuntu 15.10"
			echo "2 xUbuntu 15.04"
			echo "c Cancel"
			echo "v Voltar ao menu principal"
			read opt_ubuntu

			case $opt_ubuntu in
				1)
					txt="Instalando Tema Arc para Ubuntu versao 15.10"
					echo "$txt"
					read -p "Quer continuar ? [s|n] "

                    case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;

                        Y|y|S|s)
                            echo "Aguarde..."
                            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_15.10/ /' >> /etc/apt/sources.list.d/arc-theme.list"
                            sudo apt-get update
                            sudo apt-get install arc-theme ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
                    esac ;;
				2)
					txt="Instalando Tema Arc para Ubuntu versao 15.04"
					echo "$txt"
					reap -p "Quer continuar ? [s|n] "

                    case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
					    Y|y|S|s)
                            echo "Aguarde..."
                            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_15.04/ /' >> /etc/apt/sources.list.d/arc-theme.list"
                            sudo apt-get update
                            sudo apt-get install arc-theme ;;
					    *) OptInvalid ${FUNCNAME[ 0 ]} ;;
					esac ;;
				C|c)    Call Cancel ;;
				V|v)    Init ;;
				*)      OptInvalid ${FUNCNAME[ 0 ]} ;;

			esac ;;

		2)
			echo "Informe a versão"
			echo "1 openSUSE Tumbleweed"
			echo "2 openSUSE Leap 42.1"
			echo "3 openSUSE 13.2"
			echo "c Cancel"
			echo "v Voltar ao menu principal"
			read versao

			case $versao in
				1)
					txt="Instalando Tema Arc para openSUSE Tumbleweed"
					echo "$txt"
					read -p "Quer continuar ? [s|n] "

                    case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                        Y|y|S|s)
                            echo "Aguarde..."
                            zypper addrepo http://download.opensuse.org/repositories/home:Horst3180/openSUSE_Tumbleweed/home:Horst3180.repo
                            zypper refresh
                            zypper install arc-theme ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
					esac ;;
				2)
					txt="Instalando Tema Arc para openSUSE eap 42.1"
					echo "$txt"
					read -p "Quer continuar ? [s|n] "

                    case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                        Y|y|S|s)
                            zypper addrepo http://download.opensuse.org/repositories/home:Horst3180/openSUSE_Leap_42.1/home:Horst3180.repo
                            zypper refresh
                            zypper install arc-theme ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
					esac ;;

				3)
					txt="Instalando Tema Arc para openSUSE 13.2"
					echo "$txt"
					read -p "Quer continuar ? [s|n] "

					case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                        Y|y|S|s)
                            zypper addrepo http://download.opensuse.org/repositories/home:Horst3180/openSUSE_13.2/home:Horst3180.repo
                            zypper refresh
                            zypper install arc-theme ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
					esac ;;
				C|c) Call Cancel ;;
				V|v) Init ;;

				*) OptInvalid ${FUNCNAME[ 0 ]} ;;
			esac ;;
		3)
			echo "Informe a versão"
			echo "1 Fedora 23"
			echo "2 Fedora 22"
			echo "3 Fedora 21"
			echo "c Cancel"
			echo "v Voltar ao menu principal"

			read versao

			case $versao in
				1)
					txt="Instalando Tema Arc para Fedora 23"
					echo "$txt"
					read -p "Quer continuar ? [s|n] "

                    case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
                        Y|y|S|s)
                            echo "Aguarde..."
                            cd /etc/yum.repos.d/
                            wget http://download.opensuse.org/repositories/home:Horst3180/Fedora_23/home:Horst3180.repo
                            yum install arc-theme ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
					esac ;;
				2)
					txt="Instalando Tema Arc para Fedora 22"
					echo "$txt"
					read -p "Quer continuar ? [s|n] "

                    case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
					    Y|y|S|s)
                            echo "Aguarde..."
                            cd /etc/yum.repos.d/
                            wget http://download.opensuse.org/repositories/home:Horst3180/Fedora_22/home:Horst3180.repo
                            yum install arc-theme ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
					esac ;;
				3)
					txt="Instalando Tema Arc para Fedora 21"
					echo "$txt"
					read -p "Quer continuar ? [s|n] "

                    case $REPLY in
                        N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
					    Y|y|S|s)
                            echo "Aguarde..."
                            cd /etc/yum.repos.d/
                            wget http://download.opensuse.org/repositories/home:Horst3180/Fedora_21/home:Horst3180.repo
                            yum install arc-theme ;;
                        *) OptInvalid ${FUNCNAME[ 0 ]} ;;
					esac ;;

				C|c) Call Cancel ;;

				V|v) Pergunta ;;

				*) OptInvalid ${FUNCNAME[ 0 ]} ;;
			esac ;;
		4)
			txt="Instalando Tema Arc para Fedora 23"
			echo "$txt"
			read -p "Quer continuar ? [s|n] "

            case $REPLY in
                N|n|C|c) Cancel ${FUNCNAME[ 0 ]} "$txt";;
    			Y|Y|S|s)
                    echo "Aguarde..."
                    echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/Debian_8.0/ /' >> /etc/apt/sources.list.d/arc-theme.list
                    apt-get update
                    apt-get install arc-theme ;;
                *) OptInvalid ${FUNCNAME[ 0 ]} ;;
			esac ;;

		C|c) Call Cancel ;;

		V|v) Init ;;

		*) OptInvalid ${FUNCNAME[ 0 ]} ;;
	esac
}