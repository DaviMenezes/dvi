#!/usr/bin/env bash

#Cria um arquivo unicos com todas as funcoes
touch dvi.sh
sudo chmod 777 dvi.sh
cat scripts/Dvi.sh > dvi.sh
echo >> dvi.sh
cat scripts/SoftwareCenter.sh >> dvi.sh
echo >> dvi.sh
cat scripts/ThemeCenter.sh >> dvi.sh
echo >> dvi.sh
cat scripts/SystemMaintenance.sh >> dvi.sh
echo >> dvi.sh

echo "Call Init" >> dvi.sh

#Move o arquivo para dentro da pasta que sera enviada ao diretorio /opt/
mkdir dvi_linux
mv dvi.sh dvi_linux/
mv scripts/dvi.png dvi_linux/